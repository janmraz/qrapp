#!/bin/bash

NAME=qrweb
WD_NAME=qrwebwd
SERVICE_FILE=qrweb.sh
WATCHDOG_FILE=qrwebwd.sh
USERNAME=www-data
LOG_NAME=qr_web_app

echo "Install service"
if [ ! -w /etc/init.d ]; then
  echo "You don't gave me enough permissions to install service myself."
  echo "That's smart, always be really cautious with third-party shell scripts!"
  echo "You should now type those commands as superuser to install and run your service:"
  echo ""
  echo "   cp \"$SERVICE_FILE\" \"/etc/init.d/$NAME\""
  echo "   touch \"/var/log/$NAME.log\" && chown \"$USERNAME\" \"/var/log/$NAME.log\""
  echo "   update-rc.d \"$NAME\" defaults"
  echo "   service \"$NAME\" start"
else
  echo "1. cp \"$SERVICE_FILE\" \"/etc/init.d/$NAME\""
  cp -v "$SERVICE_FILE" "/etc/init.d/$NAME"
  echo "2. touch \"/var/log/$NAME.log\" && chown \"$USERNAME\" \"/var/log/$NAME.log\""
  touch "/var/log/$LOG_NAME.log" && chown "$USERNAME" "/var/log/$LOG_NAME.log"
  echo "3. update-rc.d \"$NAME\" defaults"
  update-rc.d "$NAME" defaults
  echo "4. service \"$NAME\" start"
  service "$NAME" start
fi

echo "DONE"
