#!/bin/sh

# chkconfig: 2345 55 10
# description:QR Web application
# processname:qrWeb

usage() {
        echo "qrweb {start|stop|restart|status|"
        exit 0
}

APP_ROOT=/usr/share/qr_web_app/qr_web_application
APP_BIN=bin

RUNAS=www-data
PIDFILE=/var/run/qr_web_app.pid
LOGFILE=/var/log/qr_web_app.log

start() {
  if [ -f /var/run/$PIDNAME ] && kill -0 $(cat /var/run/$PIDNAME); then
    echo 'Service already running' >&2
    return 1
  fi
  echo 'Starting service…' >&2
  cd $APP_ROOT
  local CMD="nodejs $APP_BIN/www &> \"$LOGFILE\" & echo \$!"
  su -c "$CMD" > "$PIDFILE"
  echo 'Service started' >&2
}

stop() {
  if [ ! -f "$PIDFILE" ] || ! kill -0 $(cat "$PIDFILE"); then
    echo 'Service not running' >&2
    return 1
  fi
  echo 'Stopping service…' >&2
  kill -15 $(cat "$PIDFILE") && rm -f "$PIDFILE"
  echo 'Service stopped' >&2
}

case $1 in

    start)
        start
        ;;
    stop)
        stop
        ;;
    status) if [ ! -f $(cat "$PIDFILE") ]; then
            echo "Running"
        else
            echo "Not running"
        fi
        ;;
    restart)
        stop
        start
        ;;
    *) usage
        ;;
esac
