#!/bin/bash

PIDNAME=qr_web_app.pid

while true
do
   sleep 60
   if [ -f /var/run/$PIDNAME ] && kill -0 $(cat /var/run/$PIDNAME); then
     echo 'service qrweb start'
   fi
done
