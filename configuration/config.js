var mysql = require('mysql');

var config_db = {
    host: '192.168.2.86',
    user: 'qr_web_app',
    password: 'P@ssw0rd',
    database: 'export_domat'
};
var config_db_pdf = {
    host:'192.168.2.75',
    user:'qrweb',
    password: 'qrweb',
    database: 'novyweb',
};
var _conn;//mysql.createConnection(config_db);
var _connpdf;//mysql.createConnection(config_db_pdf);
var conn = function() { console.log("_conn"); return _conn; };
var connpdf = function() { console.log("_connpdf"); return _connpdf; };
var handleEndConnections = function (){
    _conn.end(function() {console.log('terminating database')});
    _connpdf.end(function() {console.log('terminating pdf-database')});
};
var createConnections = function(){
    console.log('Create database connection');
    _conn = mysql.createConnection(config_db); // Recreate the connection, since the old one cannot be reused.
    console.log('Create PDF database connection');
    _connpdf = mysql.createConnection(config_db_pdf);
};
var connection;
var handleDisconnect = function() {
    _conn = mysql.createConnection(config_db); // Recreate the connection, since the old one cannot be reused.
    _connpdf = mysql.createConnection(config_db_pdf);

    _conn.connect(function(err) {
        if(err) {
            console.log('error when connecting to db:', err);
            //setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
        }
    });
    _connpdf.connect(function(err) {
        if(err) {
            console.log('error when connecting to db:', err);
            //setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
        }                                     // to avoid a hot loop, and to allow our node script to
    });                                     // process asynchronous requests in the meantime.
                                            // If you're also serving http, display a 503 error.
    //connection.on('error', function(err) {
    //    console.log('db error', err);
    //    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
    //        handleDisconnect();                         // lost due to either server restart, or a
    //    }
    //});
};
module.exports.start = createConnections;
//module.exports.end = handleEndConnections;
//module.exports.disconnect = handleDisconnect;
module.exports.connpdf = connpdf;
module.exports.conn = conn;