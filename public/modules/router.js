/**
 * Created by Johny on 13.2.2016.
 */
var router = angular.module('router', ['ui.router']);
router.config([
    '$stateProvider',
    '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('loading',{
                controller: 'LoadingCtrl',
                templateUrl: '../s/static/modules/views/loading.html'
            })
            .state('main', {
                templateUrl: '../s/static/modules/views/table.html',
                controller: 'DataCtrl'
            })
            .state('error',{
                templateUrl: '../s/static/modules/views/error.html'
            });

        $urlRouterProvider.otherwise('');
        
    }
]);