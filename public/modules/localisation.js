/**
 * Created by Johny on 13.2.2016.
 */
var localisation = angular.module('localisation',['pascalprecht.translate']);
localisation.config(['$translateProvider', function ($translateProvider) {
    $translateProvider.translations('multi', {
        'NAME': document.getElementById("NAME").innerHTML,
        'SERIAL_NUMBER': document.getElementById("SERIAL_NUMBER").innerHTML,
        'PRODUCT_NUMBER': document.getElementById("PRODUCT_NUMBER").innerHTML,
        'MAC_ADDRESS': document.getElementById("MAC_ADDRESS").innerHTML,
        'SUPPLY': document.getElementById("SUPPLY").innerHTML,
        'POWER': document.getElementById("POWER").innerHTML,
        'FIRMWARE': document.getElementById("FIRMWARE").innerHTML,
        'OS': document.getElementById("OS").innerHTML,
        'HARDWARE': document.getElementById("HARDWARE").innerHTML,
        'PDF': document.getElementById("PDF").innerHTML,
        'LOADING': document.getElementById("LOADING")
    });

    $translateProvider.preferredLanguage('multi');
    
}]);


