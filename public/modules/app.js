var app = angular.module('ClientApp', ['router','localisation']);
app.factory('DataService',function($rootScope,$http,$state){
    var service = {};
    service.GetData = function(){
        return $http.get(window.location.href.split('#')[0] + '/data').then(successCallback, errorCallback);
    };
    function successCallback (response){
        console.log(response);
        if(response.data != 'error') {
            service.data = angular.fromJson(response.data);
        }else {
            console.log('error');
        }
    }
    function errorCallback(error){
        $rootScope.$broadcast('ErrorReceived');
        console.log(error);
    }
    return service;
});
app.controller('HomeCtrl',['DataService','$state',function(DataService,$state){
    $state.go('loading');
    console.log('HomeCtrl');
}]);
app.controller('LoadingCtrl',function($state,$scope,DataService){
    DataService
        .GetData()
        .then(function () {
            $state.go('main');
        });
    console.log('loadingCtrl');
});
app.controller('DataCtrl',['DataService','$scope',function(DataService,$scope){
    $scope.data = DataService.data;
    $scope.PdfRelocation = function(){
        window.open($scope.data.linkPDF, "_blank");
    };
    console.log('data: ',$scope.data);
}]);

app.run(['$rootScope', function($rootScope) {
    console.log('Angular init');
}]);