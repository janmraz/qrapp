var express = require('express');
var favicon = require('serve-favicon');
var routes = require('./routes');

//create express app
var app = express();

//static files handler e.g. css,js,html...
app.use('/s/static', express.static(__dirname + '/public'));

// favicon handler
app.use(function(req, res, next){
  if (req.url === '/favicon.ico') {
    res.writeHead(200, {'Content-Type': 'image/x-icon'} );
    res.end(/* icon content here */);
  } else {
    next();
  }
});

// view engine setup
app.set('view engine', 'jade');
app.set('views', __dirname + '/views');

// Routes
app.use('/s',routes);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error');
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
