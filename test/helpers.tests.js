/**
 * Created by janmraz on 07/07/16.
 */
var assert = require('chai').assert;
var helpers = require('../configuration/helpers');
describe('Helpers', function() {
    describe('ifnull', function() {
        it('should return - when the value is not present', function() {
            assert.equal('-', helpers.ifnull(''));
            assert.equal('-', helpers.ifnull(null));
            assert.equal('-', helpers.ifnull(undefined));
        });
        it('should return original text', function() {
            assert.equal('ahoj', helpers.ifnull('ahoj'));
            assert.equal(345, helpers.ifnull(345));
        });
    });
    describe('ifprefixM', function() {
        it('should return unprefixed input ', function() {
            assert.equal('super', helpers.ifprefixM('M_super'));
            assert.equal('m_ahoj', helpers.ifprefixM('m_ahoj'));
            assert.equal('_ok', helpers.ifprefixM('_ok'));
            assert.equal('Mok', helpers.ifprefixM('Mok'));
        });
        it('should return original text', function() {
            assert.equal('ahoj', helpers.ifprefixM('ahoj'));
            assert.equal(345, helpers.ifprefixM(345));
        });
    });
    describe('except', function() {
        it('should return exception', function() {
            assert.equal('MXPLC', helpers.except('maxiPLC B'));
            assert.equal('MXPLC', helpers.except('M maxiPLC L'));
            assert.equal('IPCB.1', helpers.except('IPCB.10'));
            assert.equal('IPCT.1', helpers.except('IPCTL'));
            assert.equal('PWR011', helpers.except('PWR 011'));
            assert.equal('PWR010', helpers.except('PWR 010'));
        });
        it('should return original text', function() {
            assert.equal('ahoj', helpers.except('ahoj'));
            assert.equal(345, helpers.except(345));
        });
    });
});