var express = require('express');
var router = express.Router();
var helpers = require('../configuration/helpers');
var config = require('../configuration/config');
var fs = require('fs');
var rlng = 'en';

// GET /s
router.get('/', function(req,res,next){
    var rlngs = req.headers["accept-language"].toString().split(',');
    console.log('layout request from : '+ req.params.pid);
    console.log(req.headers["accept-language"].toString());
    var lngs = fs.readdirSync('./localisation');
    for(k in rlngs)
    {
        k = k[0] + k[1];
    }
    rlngs.push('en');
    var locs = {};
    for(k in lngs)
    {
        console.log('Check index: ' + k + ' for ' + lngs[k]);
        var indx = lngs[k].lastIndexOf('.');
        locs[lngs[k].substring(0, indx < 0 ? 0 : indx)] = lngs[k];
        console.log('locs[' + lngs[k].substring(0, indx < 0 ? 0 : indx) + '] = ' + lngs[k]);
    }
    console.log('Supported localisations: ' + locs);
    for(i = 0; i < rlngs.length; i++)
    {
        var l = rlngs[i].toString().substring(0,2);
        console.log('Check: ' + l);
        if (!!locs[l])
        {
            console.log(l + ' found');
            rlng = l;
            break;
        }
    }
    console.log('Selected localisation: ' + rlng);
    res.render('home', require('../localisation/'+locs[rlng]));
    return;
});
// GET /s/:pid
router.get('/:pid',function(req,res){
    var rlngs = req.headers["accept-language"].toString().split(',');
    console.log('layout request from : '+ req.params.pid);
    console.log(req.headers["accept-language"].toString());
    var lngs = fs.readdirSync('./localisation');
    for(k in rlngs)
    {
        k = k[0] + k[1];
    }
    rlngs.push('en');
    var locs = {};
    for(k in lngs)
    {
        console.log('Check index: ' + k + ' for ' + lngs[k]);
        var indx = lngs[k].lastIndexOf('.');
        locs[lngs[k].substring(0, indx < 0 ? 0 : indx)] = lngs[k];
        console.log('locs[' + lngs[k].substring(0, indx < 0 ? 0 : indx) + '] = ' + lngs[k]);
    }
    console.log('Supported localisations: ' + locs);
    for(i = 0; i < rlngs.length; i++)
    {
        var l = rlngs[i].toString().substring(0,2);
        console.log('Check: ' + l);
        if (!!locs[l])
        {
            console.log(l + ' found');
            rlng = l;
            break;
        }
    }
    console.log('Selected localisation: ' + rlng);
    res.render('layout', require('../localisation/'+locs[rlng]));
    return;
});
// GET /s/:pid/data
router.get('/:pid/data',function(req,res){
    config.start();
    var now = new Date();
    var json,link;
    console.log('(' + now + ') request for data id - ' + req.params.pid);
    var table = 'vyrobni_cisla';
    var query = 'SELECT * FROM ' + table + ' WHERE vyrobni_cislo = ' + req.params.pid;
    var conn = config.conn();
    console.log('connecting database "' + conn + '"');
    conn.connect(function(err) {
        if(err) {
            console.log('error when connecting to db:', err);
        }
    });
    conn.query(query, function(err,results,fields){
        if(err){
            console.log(err);
            res.send('error');
        }else if(results[0] === undefined){
            var som = [{vyrobni_cislo: req.params.pid,oznaceni_stitek:'-',MAC_adresa:'',napeti:'',prikon:'',verze_software:'',verze_desky:''}];
            console.log('Found nothing')
            helpers.ready(res,som,link);
        }else {
            console.log('Result is ', results[0]);
            var pdffind = results[0].oznaceni_stitek;
            var sd = helpers.except(pdffind);
            if(sd == pdffind){
                pdffind = helpers.pdfQueryParse(pdffind);
            }else{
                pdffind = sd;
            }
            console.log('To pdf find',pdffind);
            var querypdf = "SELECT b.post_id, b.meta_key, b.meta_value FROM wp_postmeta a INNER JOIN wp_postmeta b ON a.post_id = b.post_id WHERE a.meta_key='Kód produktu' AND a.meta_value LIKE '%" + pdffind + "%' AND (b.meta_key='wp_custom_attachment' OR b.meta_key='ARCHIVE' OR b.meta_key='Kód produktu')";
            var connpdf = config.connpdf();
            console.log('connecting pdf-database "' + connpdf + '"');
            connpdf.connect(function(err) {
                if(err) {
                    console.log('error when connecting to db:', err);
                }
            });
            connpdf.query(querypdf,function(err,resultspdf,fields){
              if(err) {
                console.log(err);
                return;
              }else{
                console.log(resultspdf);
                if(resultspdf.length == 0){
                    helpers.ready(res,results);
                    return;
                }
                for (key in resultspdf) {
                  console.log(key);
                  console.log(resultspdf[key]);
                  if(resultspdf[key]["meta_key"] == "wp_custom_attachment"){
                    console.log(helpers.unserialize(resultspdf[key]["meta_value"]));
                    var som1 = helpers.unserialize(resultspdf[key]["meta_value"]);
                    console.log(som1['url']);
                    var suffix = som1['url'].substring(som1['url'].lastIndexOf('_') + 1).split('.')[0];  // Parse html link. Take the last text after _ and split it with '.' char. It should be "_<code>.pdf" where code could be "<country>-<nation>"
                    suffix = suffix === 'cz' ? 'cs' : suffix;  // Catalog has cz suffix for cs language. Retype it.
                    console.log("suffix = " + suffix);
                    if(suffix.substring(0,2) == rlng) {
                        link = som1['url'];
                        console.log('LINK TO PDF IS ' + link);
                        break;
                    }
                  }
                }
                console.log('End of iteration');
                helpers.ready(res,results,link);
              }
            }).on('error',function(){
                console.log('connection pdf failed');
                helpers.ready(res,results);
            });
            connpdf.end( function(err) { console.log('PDF database connection closed with error: ' + err); } );
        }
    }).on('error',function(err){
        console.log('connection mysql failed');
		res.send('error');
    });
    conn.end( function(err) { console.log('Database connection closed with error: ' + err); } );
});
module.exports = router;
